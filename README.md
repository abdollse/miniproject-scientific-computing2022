## miniproject-scientific-computing2022

This program creates a User Interface using Textual and builds a framework with buttons and input fields. It utilizes CSS to style the widgets. The program is implemented using the Class method, which takes in data parameters, a header, and a status as input. The status can be a string that provides information about the parameters and process. The program also defines certain binding keys that perform specific actions, such as:

- D for changing the mode
- Q for quitting the mode
- TAB for transitioning the sidebar

If a Quiet/EXIT mode requests, the result will be saved in a text file as 'outfile.txt'.

## Installation
Textual requires Python 3.7 or later (if you have a choice, pick the most recent Python). Textual runs on Linux, macOS, Windows and probably any OS where Python also runs. Once you have Textual installed, run the following to get an impression of what it can do:
`python "filename.py"`


## Support
you can find additional information and #help about Textual [](https://textual.textualize.io/).
Use examples liberally, and show the expected output if you can. 


## License
Textual is licensed under MIT.

