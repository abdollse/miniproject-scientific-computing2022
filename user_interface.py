import sys
from textual.app import App, ComposeResult
from textual.widget import Widget
from textual.containers import Grid
from textual.screen import Screen
from textual.widgets import Button, Static, Input, Footer, Header
from textual.containers import Horizontal, Vertical
from textual import events
from textual.message import Message


class QuitScreen(Screen):
    def compose(self) -> ComposeResult:
        yield Grid(
            Static("Are you sure you want to quit?", id="question"),
            Button("Quit", variant="error", id="quit"),
            Button("Cancel", variant="primary", id="cancel"),
            id="dialog",
        )

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "quit":
            self.app.exit()
        else:
            self.app.pop_screen()


class UIF(App[None]):
    CSS_PATH = "user_interface.css"
    # make some action key
    BINDINGS = [
        ("d", "toggle_dark", "Toggle dark mode"),
        ("q", "request_quit", "Quit"),
        ("tab", "toggle_class('#sidebar', '-active')","side bar"),
    ]

    def __init__(self, A, head, status) -> None:
        self.A = A
        self.head = head
        self.stat = status
        super().__init__()

    def compose(self) -> ComposeResult:
        yield Header()
        yield Static(self.head, classes="header")
        yield Footer()

        for key, val in self.A.items():
            yield Grid(
                Static(key, id="label"),
                Input(value=val, placeholder=key, name=key, id="input"),
                id="bi",
            )

        yield Static(self.stat, id="sidebar")
        yield Button("EXIT", id="exit", variant="warning")

    def on_mount(self) -> None:
        """Called when app starts."""
        # Give the input focus, so we can start typing straight away
        self.query_one(Input).focus()

    def on_input_changed(self, message: Input.Changed) -> None:
        """Called when an Input changed"""
        for key, val in self.A.items():
            if message.input.name == key:
                self.A[key] = message.value

    def output(self) -> None:
        """Called to save result into a .txt file when  App send a Quit\Exit mode request"""
        outfile = open("outfile.txt", "w")
        for key, value in self.A.items():
            outfile.write(f"{key}={value}\n")

    def action_request_quit(self) -> None:
        """An action to Quit mode."""
        self.output()
        self.push_screen(QuitScreen())

    def on_button_pressed(self, event: Button.Pressed) -> None:
        """Called when button is pressed."""
        button_id = event.button.id
        """Exit requeste"""
        if button_id == "exit":
            self.output()
            self.push_screen(QuitScreen())

    def action_toggle_dark(self) -> None:
        """An action to toggle dark mode."""
        self.dark = not self.dark


UIF(
    {
        "param1": "0",
        "param2": "0",
        "param3": "0",
        "param4": "0",
    },
    head="hi, I am a New textualize",
    status="This UFI is about: Status",
).run()
